generate:
	#@protoc -I=proto --go_out=proto proto/*.proto
	#@protoc --proto_path=proto --go_out=plugins=grpc:proto/gen --go_opt=paths=source_relative proto/user.proto
	#@protoc --go_out=plugins=proto:. proto/*.proto
	#@protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative helloworld/helloworld.proto
	# https://grpc.io/docs/languages/go/quickstart/#regenerate-grpc-code
	@protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative proto/*.proto
