
links:

- https://developers.google.com/protocol-buffers/docs/gotutorial
  
- https://pkg.go.dev/google.golang.org/protobuf

importar google.golang.org/protobuf
executar: go get -t google.golang.org/protobuf

rodar para gerar os arquivos do protobuffer
executar: protoc -I=proto --go_out=proto proto/*.proto

na primeira vez que executar o comando do protobuffer irá perguntar para instalar o 'protobuf-compiler', deve-se permitir