module bitbucket.org/maironmscosta/grpc-proto-teste

go 1.16

require (
	google.golang.org/grpc v1.39.0
	google.golang.org/protobuf v1.27.1
)
