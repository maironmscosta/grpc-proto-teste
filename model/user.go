package model

type UserResponse struct {
	ID         int64
	Name       string
	Username   string
	AvatarURL  string
	Location   string
	Statistics Statistics
	ListURLs   string
	Idade      int64
}

type Statistics struct {
	Followers int64
	Following int64
	Repos     int64
	Gists     int64
}

func GetUserMap (name string) UserResponse {

	users := map[string]UserResponse{
		"Mairon": {
			ID: 123,
			Name: "Mairon Costa",
		},
		"Marilda": {
			ID: 100,
			Name: "Marilda Firmino",
		},
		"Debora": {
			ID: 200,
			Name: "Debora Maia",
		},
	}
	return users[name]
}